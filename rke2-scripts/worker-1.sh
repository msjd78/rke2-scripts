#!/bin/bash

sudo hostnamectl set-hostname worker-1

mkdir -p /etc/rancher/rke2/


# ========== Create the RKE2 config file ========== 
cat << EOF > /etc/rancher/rke2/config.yaml
node-name: worker-1
# with-node-id: true
server: https://172.31.3.46:9345
token: K10e50fb925d8801e9c877532170e810a43c0e6d36ca73aa2d1803a74591bfca7ee::server:c35ae8bae31818f7260bd70880e5638a
cni: calico
EOF


curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="agent" INSTALL_RKE2_VERSION="v1.27.9+rke2r1" sh -
systemctl enable rke2-agent.service
systemctl start rke2-agent.service
