#!/bin/bash

sudo hostnamectl set-hostname master-3

mkdir -p /etc/rancher/rke2/

# ========== Create the RKE2 config file ========== 

cat <<EOF >/etc/rancher/rke2/config.yaml
# node-name: master-3
server: https://172.31.3.46:9345
token: K10e50fb925d8801e9c877532170e810a43c0e6d36ca73aa2d1803a74591bfca7ee::server:c35ae8bae31818f7260bd70880e5638a
with-node-id: true
cni: calico
EOF


curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="server" INSTALL_RKE2_VERSION="v1.27.9+rke2r1" sh -
systemctl enable rke2-server.service
systemctl start rke2-server.service

mkdir ~/.kube
cp /var/lib/rancher/rke2/bin/kubectl /usr/local/bin
cp /etc/rancher/rke2/rke2.yaml ~/.kube/config
