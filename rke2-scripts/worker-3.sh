#!/bin/bash


mkdir -p /etc/rancher/rke2/


# ========== Create the RKE2 config file ========== 
cat << EOF > /etc/rancher/rke2/config.yaml
#node-name: worker-3
with-node-id: true
server: https://172.31.8.132:9345
token: K10f9c4ee662d473b7214099a16250f64be5136aab6700490c521e81bf28b86dbf2::server:97537bf81dd26ef7a6a45a1e200e5ebb
cni: calico
EOF


curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="agent" INSTALL_RKE2_VERSION="v1.27.9+rke2r1" sh -
systemctl enable rke2-agent.service
systemctl start rke2-agent.service
