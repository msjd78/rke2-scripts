### Deploy Cert-Manager ( One time Job ) 
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.13.3/cert-manager.yaml


### Deploy Ingress Controller with Helm ( One time Job ) 

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx 

helm repo update

helm -n ingress-nginx install ingress-nginx ingress-nginx/ingress-nginx --create-namespace
